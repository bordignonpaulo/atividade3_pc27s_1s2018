/*
 * Exemplo1: Programacao com threads
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2017
 */
package atividade3;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Paulo
 */
public class Print  implements Runnable {
    
    private String nome;
    private String cor;
    private String hora;
    
    public Print (String nome, String cor){
        this.nome = nome;
        this.cor = cor;
    }
    
    public void run(){
        
        while(true){
        switch (cor){
            case "verde":
                this.hora = new
                        SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                        format(new Date());
                System.out.println("Nome:" + this.nome + "Cor:" + this.cor + "Hora:" + this.hora);
                this.cor="amarelo";  
                break;
                
            case "amarelo":
                this.hora = new
                        SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                        format(new Date());
                System.out.println("Nome:" + this.nome + "Cor:" + this.cor + "Hora:" + this.hora);                
                this.cor="vermelho";
                break;
                
            case "vermelho":
                this.hora = new
                        SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                        format(new Date());
                System.out.println("Nome:" + this.nome + "Cor:" + this.cor + "Hora:" + this.hora);
                this.cor="verde";
                break;
                
            default:
            break;
        } // fim switch
        } // fim while
    } // fim run   
}