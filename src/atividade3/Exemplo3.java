/**
 * Exemplo1: Programacao com threads
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2017
 */
package atividade3;
/**
 *
 * @author Paulo
 */
public class Exemplo3  {

        public static void main(String [] args){
                        
            System.out.println("Inicio da criacao das threads.");
            
            //Cria cada thread com um novo runnable selecionado
            Thread t1 = new Thread(new Print("thread1","verde"));            
            Thread t2 = new Thread(new Print("thread2","amarelo")); 
            Thread t3 = new Thread(new Print("thread3","vermelho")); 
            
            //Inicia as threads, e as coloca no estado EXECUTAVEL
            t1.start(); //invoca o método run de t1
            t2.start(); 
            t3.start(); 
            
            System.out.println("Threads criadas");
        }       
}